package org.fugerit.java.sample.conn;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.Properties;

import org.fugerit.java.core.lang.helpers.StringUtils;
import org.fugerit.java.core.util.result.BasicResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleTester {

	private final static Logger logger = LoggerFactory.getLogger( SimpleTester.class );
	
	public int conn( Properties params ) throws Exception {
		int res = BasicResult.RESULT_CODE_OK;
		String url = params.getProperty( ConnTest.ARG_URL );
		String data = readUrlData(url, params);
		logger.info( "stream data -> "+data );
		logger.info( "res : "+res+" -> url : "+url );
		return res;
	}
	
	private static String readUrlData( String url, Properties params ) throws Exception {
		final String proxyHost = params.getProperty( ConnTest.ARG_PROXY_HOST );
		final String proxyPort = params.getProperty( ConnTest.ARG_PROXY_PORT );
		final String proxyUser = params.getProperty( ConnTest.ARG_PROXY_USER );
		final String proxyPass = params.getProperty( ConnTest.ARG_PROXY_PASS );
		HttpURLConnection conn;
		if ( !StringUtils.isEmpty( proxyHost ) && !org.fugerit.java.core.lang.helpers.StringUtils.isEmpty( proxyPort ) ) {
			logger.info( "using proxy : "+proxyHost+":"+proxyPort+" (user:"+proxyUser+")" );
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, Integer.parseInt( proxyPort )));
			if ( !StringUtils.isEmpty( proxyUser ) && !StringUtils.isEmpty( proxyPass ) ) {
				Authenticator authenticator = new Authenticator() {
			        public PasswordAuthentication getPasswordAuthentication() {
			            return (new PasswordAuthentication( proxyUser, proxyPass.toCharArray() ) );
			        }
			    };
			    Authenticator.setDefault(authenticator);
			}
			URL u = new URL( url );
			conn = (HttpURLConnection)u.openConnection( proxy );
		} else {
			URL u = new URL( url );
			conn = (HttpURLConnection)u.openConnection();
		}
		StringBuffer buffer = new StringBuffer();
		if ( conn.getResponseCode() != 200 ) {
			throw new Exception( "HTTP exit code : "+conn.getResponseCode() );
		} else {
			BufferedReader br = new BufferedReader( new InputStreamReader( conn.getInputStream() ) );
			String line = br.readLine();
			while ( line != null ) {
				buffer.append( line );
				line = br.readLine();
			}
			br.close();
		}
		conn.disconnect();
		return buffer.toString();
	}
	
}
