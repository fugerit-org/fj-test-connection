package org.fugerit.java.sample.conn;

import java.util.Properties;

import org.fugerit.java.core.cli.ArgUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnTest {

	private final static Logger logger = LoggerFactory.getLogger( ConnTest.class );
	
	public final static String ARG_URL = "url";
	
	public final static String ARG_PROXY_HOST = "proxy.host";
	public final static String ARG_PROXY_PORT = "proxy.port";
	public final static String ARG_PROXY_USER = "proxy.user";
	public final static String ARG_PROXY_PASS = "proxy.pass";
	
	public static void main( String[] args ) {
		try {
			Properties params = ArgUtils.getArgs( args , true, false );
			SimpleTester tester = new SimpleTester();
			logger.info( "Test result : "+tester.conn( params ) );
		} catch (Exception e) {
			logger.error( "Error : "+e, e );
		}
	}
	
}
