# Quick start

## Requirements 
java version : 1.8+ (for running)
mvn 3+ (for building)

## Parameters
url[required] : connection url (es. 'http://test.fugerit.org')
proxy.host[optional] : proxy host
proxy.port[optional] : proxy port
proxy.user[optional] : proxy user
proxy.pass[optional] : proxy pass

## Build
mvn clean install -P singlepackage

## Run
java -jar target/dist-fj-test-connection-X-X-X.jar --url ${url}
or with param file
java -jar target/dist-fj-test-connection-X-X-X.jar --param-file ${path-to-properties}
